package net.pirsquare.cellotvcourt.utils;

import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by kampolleelaporn on 3/22/16.
 */
public class DateHelper {
    public static DateTimeFormatter formatFully = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZZ");
    public static DateTimeFormatter formatShowInQueuePaper = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm");
    public static DateTimeFormatter formatDateFile = DateTimeFormat.forPattern("yyyyMMddHHmmss");
    public static DateTimeFormatter formatDateFolder = DateTimeFormat.forPattern("yyyyMMdd");
    public static DateTimeFormatter formatDateReport = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm");
    public static DateTimeFormatter formatTime = DateTimeFormat.forPattern("HH:mm");

    public static String getCurrent() {
        return formatFully.print(new DateTime());
    }

    //(int year, int monthOfYear, int dayOfMonth, int hourOfDay, int minuteOfHour)
    public static String getDateFromTime(int hours, int min) {
        DateTime time = new DateTime();
        return formatFully.print(new DateTime(time.getYear(), time.getMonthOfYear(), time.getDayOfMonth(), hours, min));
    }

    public static Date getCurrentDate() {
        return new DateTime().toDate();
    }

    public static DateTime getCurrentDateTime() {
        return new DateTime();
    }

    public static String getYesterday() {
        return formatFully.print(new DateTime().minusDays(1));
    }

    public static String getDateTimeStringFromDateTime(DateTime date) {
        return formatShowInQueuePaper.print(date);
    }

    public static DateTime getResetTime() {
        return DateTime.now().withTime(4, 0, 0, 0);
    }

    public static long getMilliseconds() {
        return new DateTime().getMillis();
    }

    public static Date getMidnight() {
        return DateTime.now().withZone(DateTimeZone.forID("Asia/Bangkok")).withTime(0, 0, 0, 0).toDate();
    }

    public static String getExpireTime(String createAt) {
        return formatFully.print(new DateTime(createAt).plusHours(1));
    }

    public static String getTimetoShowInQueuePaper(String date) {
        return formatShowInQueuePaper.print(new DateTime(date));
    }

    public static String getCurrentDateForFileName() {
        return formatDateFile.print(new DateTime());
    }

    public static String getCurrentDateForFolder() {
        return formatDateFolder.print(new DateTime());
    }

    public static String getTimeReport(long timestamp) {
        return formatDateReport.print(timestamp);
    }

    public static Date getDateFromString(String date) {
        return new DateTime(date).toDate();
    }

    public static DateTime getDateTimeFromString(String date) {
        return new DateTime(date);
    }

    public static int getHourFromTimeStamp(int timeStamp) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getDefault());
        cal.setTimeInMillis(timeStamp * 1000);

        int c = cal.get(Calendar.HOUR);
        Log.e("c", c + "  " + timeStamp + "");
        return cal.get(Calendar.HOUR);
    }

    public static int getMinFromTimeStamp(int timeStamp) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getDefault());
        cal.setTimeInMillis(timeStamp * 1000);

        return cal.get(Calendar.MINUTE);
    }
}
