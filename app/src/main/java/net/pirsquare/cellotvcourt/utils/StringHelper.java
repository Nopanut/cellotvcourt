package net.pirsquare.cellotvcourt.utils;

import android.text.TextUtils;

/**
 * Created by kung on 12/28/16.
 */

public class StringHelper {
    public static String des = "ช่องบริการ ";

    public static String getDestinationNumber(String destination) {
        if (!TextUtils.isEmpty(destination))
            return destination.substring(des.length());
        else
            return "0";
    }
}
