package net.pirsquare.cellotvcourt.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonHelper {
    private static Gson gson;

    public static Gson getGson() {
        if (gson == null) {
            gson = new GsonBuilder().setLenient().enableComplexMapKeySerialization().serializeNulls().create();
        }
        return gson;
    }
}