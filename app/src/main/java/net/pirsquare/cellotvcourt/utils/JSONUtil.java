package net.pirsquare.cellotvcourt.utils;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by katopz on 2014-10-07.
 */
public class JSONUtil {
    public static JSONObject toJSONObject(String jsonString)
    {
        try {
            return new JSONObject(jsonString);
        } catch (JSONException e) {
            Log.e("JSONUtil", e.toString());
            e.printStackTrace();
        }

        return null;
    }

    public static String stringify(JSONObject jsonObject) {
        return jsonObject.toString();
    }
}
