package net.pirsquare.cellotvcourt.model;

import java.util.List;

/**
 * Created by kung on 12/7/16.
 */

public class QueueStatusChanged {


    /**
     * status : call
     * actions : ["accepted","call"]
     * remain : 1
     * extras : {"channels":{"_id":"PwuyiwAcxhGS7HtTW","name":"ช่องบริการ 1"}}
     * destination : ช่องบริการ 1
     */

    private String status;
    private int remain;
    private ExtrasBean extras;
    private String destination;
    private List<String> actions;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getRemain() {
        return remain;
    }

    public void setRemain(int remain) {
        this.remain = remain;
    }

    public ExtrasBean getExtras() {
        return extras;
    }

    public void setExtras(ExtrasBean extras) {
        this.extras = extras;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public List<String> getActions() {
        return actions;
    }

    public void setActions(List<String> actions) {
        this.actions = actions;
    }

    public static class ExtrasBean {
        /**
         * channels : {"_id":"PwuyiwAcxhGS7HtTW","name":"ช่องบริการ 1"}
         */

        private ChannelsBean channels;

        public ChannelsBean getChannels() {
            return channels;
        }

        public void setChannels(ChannelsBean channels) {
            this.channels = channels;
        }

        public static class ChannelsBean {
            /**
             * _id : PwuyiwAcxhGS7HtTW
             * name : ช่องบริการ 1
             */

            private String _id;
            private String name;

            public String get_id() {
                return _id;
            }

            public void set_id(String _id) {
                this._id = _id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}
