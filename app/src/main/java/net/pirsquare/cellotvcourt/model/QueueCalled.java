package net.pirsquare.cellotvcourt.model;

import java.util.List;

/**
 * Created by Nut on 12/7/16.
 */

public class QueueCalled {

    /**
     * service : {"id":"zajm3K9fJz","name":"รับฟ้องแพ่ง"}
     * status : call
     * refCode : pJwijNv
     * actions : ["accepted","update","call","call"]
     * createdAt : {"$date":1482901878664}
     * remain : 1
     * group : B
     * index : 3
     * user : {"providers":{}}
     * capacity : 1
     * extras : {"channels":{"_id":"PwuyiwAcxhGS7HtTW","name":"ช่องบริการ 1"}}
     * destination : ช่องบริการ 1
     */

    private ServiceBean service;
    private String status;
    private String refCode;
    private CreatedAtBean createdAt;
    private int remain;
    private String group;
    private String index;
    private UserBean user;
    private int capacity;
    private ExtrasBean extras;
    private String destination;
    private List<String> actions;

    public ServiceBean getService() {
        return service;
    }

    public void setService(ServiceBean service) {
        this.service = service;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRefCode() {
        return refCode;
    }

    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

    public CreatedAtBean getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(CreatedAtBean createdAt) {
        this.createdAt = createdAt;
    }

    public int getRemain() {
        return remain;
    }

    public void setRemain(int remain) {
        this.remain = remain;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public ExtrasBean getExtras() {
        return extras;
    }

    public void setExtras(ExtrasBean extras) {
        this.extras = extras;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public List<String> getActions() {
        return actions;
    }

    public void setActions(List<String> actions) {
        this.actions = actions;
    }

    public static class ServiceBean {
        /**
         * id : zajm3K9fJz
         * name : รับฟ้องแพ่ง
         */

        private String id;
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class CreatedAtBean {
        /**
         * $date : 1482901878664
         */

        private long $date;

        public long get$date() {
            return $date;
        }

        public void set$date(long $date) {
            this.$date = $date;
        }
    }

    public static class UserBean {
        /**
         * providers : {}
         */

        private ProvidersBean providers;

        public ProvidersBean getProviders() {
            return providers;
        }

        public void setProviders(ProvidersBean providers) {
            this.providers = providers;
        }

        public static class ProvidersBean {
        }
    }

    public static class ExtrasBean {
        /**
         * channels : {"_id":"PwuyiwAcxhGS7HtTW","name":"ช่องบริการ 1"}
         */

        private ChannelsBean channels;

        public ChannelsBean getChannels() {
            return channels;
        }

        public void setChannels(ChannelsBean channels) {
            this.channels = channels;
        }

        public static class ChannelsBean {
            /**
             * _id : PwuyiwAcxhGS7HtTW
             * name : ช่องบริการ 1
             */

            private String _id;
            private String name;

            public String get_id() {
                return _id;
            }

            public void set_id(String _id) {
                this._id = _id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}
