package net.pirsquare.cellotvcourt.model;

import com.google.gson.annotations.SerializedName;

public enum QueueAction {

    //Mobile จองคิว
    @SerializedName("reserved")
    RESERVE("reserved"),
    //tablet จะตอบกลับว่ารับทราบคิว ที่ Mobile จองเข้ามา
    @SerializedName("accepted")
    ACCEPT("accepted"),
    //tablet เรียกคิว
    @SerializedName("call")
    CALL("call"),
    //tablet พักคิว
    @SerializedName("hold")
    HOLD("hold"),
    //คิวเข้าร้านเรียบร้อย
    @SerializedName("claim")
    CLAIM("claim"),
    //Mobile กดยกเลิกคิว
    @SerializedName("cancel")
    CANCEL("cancel"),
    //tablet ตอบยืนยันการยกเลิกคิว
    @SerializedName("cancelled")
    CANCELLED("cancelled"),
    //tablet reset คิว
    @SerializedName("deleted")
    DELETED("deleted"),
    //คิวหมดอายุ
    @SerializedName("expired")
    EXPIRED("expired"),

    @SerializedName("update")
    UPDATE("update");

    private final String value;

    QueueAction(String accept) {
        this.value = accept;
    }

    public String getValue() {
        return value;
    }

    public static QueueAction getQueueStatus(String value) {
        switch (value) {
            case "reserved":
                return RESERVE;
            case "call":
                return CALL;
            case "hold":
                return HOLD;
            case "claim":
                return CLAIM;
            case "accepted":
                return ACCEPT;
            case "cancel":
                return CANCEL;
            case "cancelled":
                return CANCELLED;
            case "deleted":
                return DELETED;
            case "expired":
                return EXPIRED;
            case "update":
                return UPDATE;
        }
        return null;
    }
}